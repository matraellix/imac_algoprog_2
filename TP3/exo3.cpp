#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        //SearchTreeNode Node_initial;
        //Node_initial->value = value;
        this->value = value;
        this->left = nullptr;
        this->right = nullptr;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        //SearchTreeNode Node_new;
        if(value > this->value){
            if(this->right == nullptr){
                SearchTreeNode* Node_new = new SearchTreeNode(value);
                this->right = Node_new;
            }else{
                this->right->insertNumber(value);
            }
            

        }else if(value < this->value){
            if(this->left == nullptr){
                SearchTreeNode* Node_new = new SearchTreeNode(value);
                this->left = Node_new;
            }else{
               this->left->insertNumber(value);
            }
             
        }
        
        
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        //if(this->left != nullptr || this->right != nullptr){
        if(this->left != nullptr && this->right != nullptr){
            int lh, rh;
            lh = this->left->height();
            rh = this->right->height();
            if (lh >= rh){
                return lh + 1;
            }
            else{
                return rh + 1;
            }
        }else if(this->left != nullptr){
            return this->left->height() + 1;
        }else if(this->right != nullptr){
            return this->right->height() + 1;
        }else{
            return 1;
        }


    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if(this->left != nullptr && this->right != nullptr){
           return this->left->nodesCount() + this->right->nodesCount() +1;
        }else if(this->left != nullptr){
            return this->left->nodesCount() + 1;
        }else if(this->right != nullptr){
            return this->right->nodesCount() + 1;
        }else{
            return 1;
        }
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if(this->left == nullptr && this->right == nullptr){
            return true;
        }
        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if(this->isLeaf()){
            leaves[leavesCount] = this;
            leavesCount++;
        }
        if(this->left != nullptr){
            this->left->allLeaves(leaves, leavesCount);

        }else if(this->right != nullptr){
            this->right->allLeaves(leaves, leavesCount);
        }

	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if(this->isLeaf()){
            nodes[nodesCount] = this;
            nodesCount++;
        }else{
            //on compte les enfants de gauches
            if(this->left != nullptr) {
                this->left->inorderTravel(nodes, nodesCount);
            }

            //on compte le parent après les enfants de gauche
            nodes[nodesCount] = this;
            nodesCount++;

            //on compte les enfants de droite
            if (this->right != nullptr) {
                this->right->inorderTravel(nodes, nodesCount);
            }
        }
    }

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
	    //On compte d'abord forcément le parent
        nodes[nodesCount] = this;
        nodesCount++;

        //recursive sur d'abord les enfants à gauche puis les enfants à droite
        if (this->left != nullptr) {
            this->left->preorderTravel(nodes, nodesCount);
        }

        if (this->right != nullptr) {
            this->right->preorderTravel(nodes, nodesCount);
        }
    }

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
	
    if(this->isLeaf()){
            nodes[nodesCount] = this;
            nodesCount++;
        }else{
            //on compte les enfants de gauches
            if(this->left != nullptr) {
                this->left->postorderTravel(nodes, nodesCount);
            }


            //on compte les enfants de droite
            if (this->right != nullptr) {
                this->right->postorderTravel(nodes, nodesCount);
            }


            //on compte le parent après les enfants de gauche et de droite
            
            nodes[nodesCount] = this;
            nodesCount++;
        }
    }

	Node* find(int value) {
        // find the node containing value
        
        if(this->value != value){
            
            if (this->left != nullptr && value < this->value )
            return this->left->find(value);
            else if (this->right != nullptr && value > this->value)
            return this->right->find(value);
            
        }else{
            return this;
        }
		return nullptr;

	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
