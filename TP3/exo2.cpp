#include "tp3.h"
#include <QApplication>
#include <time.h>


MainWindow* w = nullptr;
using std::size_t;

/**
 * @brief define indexMin and indexMax as the first and the last index of toSearch
 * @param array array of int to process
 * @param toSearch value to find
 * @param indexMin first index of the value to find
 * @param indexMax last index of the value to find
 */

int binarySearchRight(Array& array, int toSearch)
{
	int start = 0;
	int end = array.size();
	int mid = end/2;
	int trouve = 0;
	while(start<end){
		
		int mid_elem = array[mid];

		if(toSearch>mid_elem){
			start = mid+1;
			
		}else if (toSearch <mid_elem){
			end = mid;	
		}else{
			trouve = mid;
			start=mid+1;
		}
		mid=start+(end-start)/2;
	}

	if(trouve == -1){
		return end;
	}
	if(trouve < array.size() && array[trouve] == toSearch){
		return trouve;
	}
	return -1;
}
int binarySearchLeft(Array& array, int toSearch)
{
	int start = 0;
	int end = array.size();
	int mid = end/2;
	int trouve = 0;
	
	while(start<end){
		
		int mid_elem = array[mid];

		if(toSearch>mid_elem){
			start = mid+1;
			
		}else if (toSearch <mid_elem){
			end = mid;	
		}else{
			trouve = mid;
			end=mid;
		}
		mid=start+(end-start)/2;
	}

	if(trouve < array.size() && array[trouve] == toSearch){
		return trouve;
	}
	return -1;
}

void binarySearchAll(Array& array, int toSearch, int& indexMin, int& indexMax)
{
	// do not use increments, use two different binary search loop
    indexMin = indexMax = -1;
	indexMin= binarySearchLeft(array, toSearch);
	indexMax= binarySearchRight(array, toSearch);


	/*
	// avec des incrémentations 
	indexMin = indexMax = -1;
	int n= binarySearch(array, toSearch);
	if(n!=-1){
		indexMin = indexMax = n;
        while (indexMax < array.size()-1 && array[indexMax+1]== toSearch){
            indexMax++;
        }
        while (indexMin > 0 && array[indexMin-1]== toSearch ) {
            indexMax--;
        }
	}
	*/
	
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchAllWindow(binarySearchAll);
	w->show();

	return a.exec();
}
