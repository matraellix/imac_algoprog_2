#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    Noeud* dernier;
    // your code
};

struct DynaTableau{
    int* donnees;
    int NbVal;
    int capacite;
    // your code
};


void initialise(Liste* liste)
{
    liste->premier = nullptr;
    liste->dernier = nullptr;
}

bool est_vide(const Liste* liste)
{
    if(liste->premier==nullptr){
        return true;
    }else{
        return false;
    }
    
}

void ajoute(Liste* liste, int valeur)
{
    Noeud * premierNoeud = new Noeud;
    premierNoeud->donnee = valeur;
    premierNoeud->suivant= nullptr;

    if (liste==nullptr)
    {
        cout<<"memory run out"<<endl;
        exit(1);
    }
    if(liste->premier==nullptr){
        liste->premier= premierNoeud;
        liste->dernier= premierNoeud;
    }else{
        liste->dernier->suivant=premierNoeud;
        liste->dernier=premierNoeud;
    }

    
}

void affiche(const Liste* liste)
{
    Noeud *actuel = liste->premier;

    while (actuel != nullptr )
    {
        cout << actuel->donnee <<endl;
        actuel = actuel->suivant;
    }
    
}

int recupere(const Liste* liste, int n)
{
    int cpt=0;
    Noeud *actuel = liste->premier;
    while (actuel != nullptr || cpt<n)
    {
        
        if(cpt==n){
           
            return actuel->donnee;
        }
        cpt++;
        actuel = actuel->suivant;
    }
    return 0;
}

int cherche(const Liste* liste, int valeur)
{
    int cpt=0;
    Noeud *actuel = liste->premier;
    while (actuel != nullptr || cpt<valeur)
    {
        
        if(valeur==actuel->donnee){
            
            return actuel->donnee;
        }
        cpt++;
        actuel = actuel->suivant;
    }
  
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    int cpt=0;
    Noeud *actuel = liste->premier;
    while (actuel != nullptr || cpt<n)
    {
        
        if(cpt==n){
            actuel->donnee= valeur;
        }
        cpt++;
        actuel = actuel->suivant;
    }
}

void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite = capacite;
    tableau->donnees = (int *) malloc(sizeof(int)*capacite);
    tableau->NbVal = 0;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->capacite > tableau->NbVal){
        tableau->donnees[tableau->NbVal]=valeur;
        tableau->NbVal++;
    } else {

        DynaTableau * NouveauTab= new DynaTableau;
        initialise(NouveauTab, (tableau->capacite)*2);
        for(int i=0; i < tableau->NbVal; i++){
            NouveauTab->donnees[i] = tableau->donnees[i];
            NouveauTab->NbVal++;
        }

        NouveauTab->donnees[NouveauTab->NbVal]=valeur;
    }
}


bool est_vide(const DynaTableau* liste)
{
    if (liste->NbVal==0){

        return true;
    }

    return false;
}

void affiche(const DynaTableau* tableau)
{
    for (int i=0; i< tableau->NbVal; i++){

        std::cout << "donnees numero: " << i << " : " << tableau->donnees[i] << std::endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if (!est_vide(tableau)){

        return tableau->donnees[n];
    }
    return 0;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    if (!est_vide(tableau)){

        for (int i=0; i< tableau->NbVal; i++){
            if (tableau->donnees[i]==valeur){

                return i;
            }
        }

    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if (!est_vide(tableau)){
        tableau->donnees[n]=valeur;
    } else {
        tableau->donnees[0]=valeur;
        tableau->NbVal++;
    }
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    Noeud * premierNoeud = new Noeud;
    if (liste == NULL || premierNoeud == NULL)
    {
        exit(EXIT_FAILURE);
    }

    premierNoeud->donnee = valeur;
    premierNoeud->suivant = NULL;

    if (liste->premier != NULL) /* La file n'est pas vide */
    {
        /* On se positionne à la fin de la file */
        Noeud * noeudActuel = liste->premier;
        while (noeudActuel->suivant != NULL)
        {
            noeudActuel = noeudActuel->suivant;
        }
        noeudActuel->suivant = premierNoeud;
    }
    else /* La file est vide, notre élément est le premier */
    {
        liste->premier = premierNoeud;
    }
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
     if (liste == NULL)
    {
        exit(EXIT_FAILURE);
    }

    int nombreDeFile = 0;
     if (liste->premier != NULL)
    {
        Noeud *elementDeFile = liste->premier;


        nombreDeFile = elementDeFile->donnee;
        liste->premier = elementDeFile->suivant;
        delete elementDeFile;
        
    }
    return nombreDeFile;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud * premierNoeud = new Noeud;
    if (liste == NULL || premierNoeud == NULL)
    {
        exit(EXIT_FAILURE);
    }

    premierNoeud->donnee = valeur;
    premierNoeud->suivant = liste->premier;
    liste->premier = premierNoeud;

}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
        if (liste == NULL)
    {
        exit(EXIT_FAILURE);
    }

    int nombreDepile = 0;
    Noeud *elementDepile = liste->premier;

    if (liste != NULL && liste->premier != NULL)
    {
        nombreDepile = elementDepile->donnee;
        liste->premier = elementDepile->suivant;
        delete elementDepile;
    }

    return nombreDepile;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
