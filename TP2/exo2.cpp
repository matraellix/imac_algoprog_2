#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	Array& sorted=w->newArray(toSort.size());
	sorted[0]=toSort[0];
	// insertion sort from toSort to sorted
	int cpt=1;
	
	for(int n=1; n<toSort.size(); n++){
		int m=0;
		for(;m<cpt; m++){
			if(toSort[n]<sorted[m]){
				sorted.insert(m,toSort[n]);
				cpt++;
				break;
			}
			
		}
		if(m==cpt){
			sorted.insert(cpt,toSort[n]);
			cpt++;
		}
		
	}

	toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
