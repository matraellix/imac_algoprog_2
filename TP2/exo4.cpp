#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int size)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	
	Array& lowerArray = w->newArray(size);
	Array& greaterArray= w->newArray(size);
	int lowerSize = 0, greaterSize = 0; // effectives sizes

	// split
	//std::cout << size << std::endl;
	
	if(size>1){
		int le_pivot= toSort[0];
		int i=1;
		while(i<size){
			if (toSort[i]>le_pivot){
                greaterArray.insert(greaterSize, toSort[i]);
                greaterSize++;
            }
            else {
                lowerArray.insert(lowerSize, toSort[i]);
                lowerSize++;
            }
			i++;
		}
	
	// recursiv sort of lowerArray and greaterArray
	recursivQuickSort(lowerArray, lowerSize);
	recursivQuickSort(greaterArray, greaterSize);
	// merge

		//lowerArray
		int j = 0;
        while (j < lowerSize){
            toSort[j] = lowerArray[j];
            j++;

        }

		//Le Pivot
        toSort[j] = le_pivot;
        j++;

		//greaterArray
        int k =0;
        while (k < greaterSize) {
            toSort[j+k] = greaterArray[k];
            k++;
        }
	}
}

void quickSort(Array& toSort){
	recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
