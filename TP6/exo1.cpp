#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */
    for (int i=0; i<nodeCount ;i++) {
            GraphNode* noeud= new GraphNode(i);
            this->appendNewNode(noeud);

        }

    for (int k=0; k<nodeCount ;k++) {

        for (int j=0; j<nodeCount ;j++) {
            if (adjacencies[k][j]!=0){
                this->nodes[k]->appendNewEdge(this->nodes[j],adjacencies[k][j]);
            }
        }

    }
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */
    int value = first->value;
    Edge* edge = first->edges;

    if (visited[value]==false){
        
        visited[value]=true;

        while(edge!=nullptr){
            this->deepTravel(edge->destination, nodes, nodesSize, visited );
            edge = edge->next;
        }
    }

}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */
	std::queue<GraphNode*> nodeQueue;
	nodeQueue.push(first);

    while(nodeQueue.size() != 0){
        visited[nodeQueue.front()->value]=true;
        nodeQueue.pop();
    }
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/
    visited[first->value] = true;
    Edge* edge = first->edges;
    while(edge!=nullptr){

        if (!visited[edge->destination->value]) {
            if (detectCycle(edge->destination, visited)){

                return true;
            }
        }
        else if (visited[edge->destination->value]){

            return true;
        }
        edge = edge->next;
    }

    return false;




}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
